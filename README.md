# Drivers Arduino uno Clon

## Introducción

Si adquiriste un clon de Arduino Uno con el integrado serial CH340, debes instalar manualmente los drivers serial para lograr la comunicación entre el Arduino y el computador.

## Uso

Windows :

```
Extraer y descomprimir los archivos del .zip
```

```
Abrir la carpeta con el nombre de CH341SER 
```

```
Ejecutar el archivo setup.exe
```


## Referencias

* [CH341SER](http://www.wch.cn/download/CH341SER_EXE.html) - Pagina oficial de descarga

## Autor

* **Edermar Dominguez** - [Ederdoski](https://github.com/ederdoski)
